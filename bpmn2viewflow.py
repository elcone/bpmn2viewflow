import sys
import random
import re
import string

from xml.etree import ElementTree as ET


# Tags that can be translated to Viewflow nodes
BPMN_SUPPORTED_TAGS = (
    'startEvent',
    'endEvent',
    'task',
    'userTask',
    'intermediateThrowEvent',
    'scriptTask',
    'serviceTask',
    'exclusiveGateway',
    'inclusiveGateway',
    'parallelGateway',
)

# Known tags than are explicitly ignored and not processed
BPMN_IGNORED_TAGS = (
    'documentation',
    'laneSet',
)

BPMN_NAMESPACE = { 'model': 'http://www.omg.org/spec/BPMN/20100524/MODEL' }

INVALID_ATTRIBUTES_CHARACTERS = re.compile('[\W ]')


def get_base_tag(tag):
    """Remove the namespace from a BPMN tag.
    """
    return tag.split('}')[1]


def get_simple_hash():
    """Generate a 8 character hash for using it as a node name.
    """
    return ''.join(random.choices(string.ascii_letters + string.digits, k=8))


def indent(size=1):
    size = size * 4
    return ' ' * size


# Viewflow's nodes types and the node class
class ViewFlowTaskTypes(object):
    START = 'Start'
    END = 'End'
    HUMAN = 'View'
    JOB = 'Job'
    FUNC = 'Handler'
    SPLIT = 'Split'
    JOIN = 'Join'
    IF = 'If'
    SWITCH = 'Switch'


# BPMN tag that is equivalent to a Viewflow's node class
VIEWFLOW_BPMN_EQUIVALENTS = {
    'startEvent': ViewFlowTaskTypes.START,
    'endEvent': ViewFlowTaskTypes.END,
    'task': ViewFlowTaskTypes.HUMAN,
    'userTask': ViewFlowTaskTypes.HUMAN,
    'intermediateThrowEvent': ViewFlowTaskTypes.HUMAN,
    'scriptTask': ViewFlowTaskTypes.FUNC,
    'serviceTask': ViewFlowTaskTypes.FUNC,
    'exclusiveGateway': ViewFlowTaskTypes.SWITCH,
    'parallelGateway': ViewFlowTaskTypes.SPLIT,
    'inclusiveGateway': ViewFlowTaskTypes.SPLIT,
}


class BPMNFile:
    """Class for holding all the processes in a bpmn file.
    """
    root = None
    filename = None
    processes = {}

    def __init__(self, filename=None):
        if not filename:
            raise Exception('File not provided!')

        self.filename = filename
        self.parse()

    def parse(self):
        tree = ET.parse(self.filename)
        self.root = tree.getroot()
        processes = self.root.findall('./model:process', BPMN_NAMESPACE)

        for p in processes:
            process = Process(bpmn_node=p)
            if process.is_valid:
                self.processes[process.id] = process


class BPMNNode:
    """Base class for the different types of nodes, every subclass
    must implement the method `parse`.
    """
    bpmn_node = None
    id = None
    name = None
    process = None

    def __init__(self, bpmn_node=None, process=None):
        self.bpmn_node = bpmn_node
        self.process = process
        self.id = self.bpmn_node.get('id')
        self.name = self.bpmn_node.get('name')
        self.parse()

    def __str__(self):
        return '{} ({})'.format(self.name, self.id)

    def parse(self):
        raise NotImplementedError

    def string_as_attribute(self, string):
        return INVALID_ATTRIBUTES_CHARACTERS.sub('_', string).lower()


class Process(BPMNNode):
    nodes = {}
    sequences = {}
    _start = None
    _generated_nodes = []
    _handlers = []

    def parse(self):
        for n in self.bpmn_node:
            base_tag = get_base_tag(n.tag)

            if base_tag in BPMN_SUPPORTED_TAGS:
                node = Node(bpmn_node=n, process=self)
                self.nodes[node.id] = node

            elif base_tag == 'sequenceFlow':
                sequence = Sequence(bpmn_node=n, process=self)
                self.sequences[sequence.id] = sequence

            elif base_tag in BPMN_IGNORED_TAGS:
                pass

            else:
                sys.stdout.write('# Not suported node found "{}".\n'.format(base_tag))

    @property
    def start(self):
        if self._start is None:
            self._start = [n for n in self.nodes.values() if n.type == ViewFlowTaskTypes.START]
        return self._start

    @property
    def is_valid(self):
        return True if self.nodes else False

    def get_viewflow_class(self):
        viewflow_class = (
            'from viewflow import flow, frontend\n'
            'from viewflow.base import Flow, this\n'
            'from viewflow.flow.views import CreateProcessView, UpdateProcessView')
        viewflow_class += '\n' * 3
        viewflow_class += '@frontend.register\n'

        sys.stdout.write(viewflow_class + 'class {}BaseFlow(Flow):\n'.format(self.name))
        for start_node in self.start:
            self.get_node_viewflow_definition(start_node)

        sys.stdout.write('\n'.join(self._handlers))

        self._generated_nodes = []

    def get_node_viewflow_definition(self, node):
        if node.id in self._generated_nodes:
            return

        self._generated_nodes.append(node.id)
        node.get_viewflow_definition()

        for n in node.next:
            self.get_node_viewflow_definition(n)


class Node(BPMNNode):
    type = None
    _next = None

    def parse(self):
        base_tag = get_base_tag(self.bpmn_node.tag)
        self.name = self.string_as_attribute(self.name) or base_tag + '_' + get_simple_hash()
        self.type = VIEWFLOW_BPMN_EQUIVALENTS[base_tag]

        is_converging_gateway = 'Gateway' in base_tag and self.bpmn_node.get('gatewayDirection') == 'Converging'
        if is_converging_gateway:
            self.type = ViewFlowTaskTypes.JOIN

    @property
    def next(self):
        if self._next is None:
            self._next = [self.process.nodes[s.target] for s in self.process.sequences.values() if s.source == self.id]
        return self._next

    @property
    def require_permission(self):
        return self.type in (ViewFlowTaskTypes.HUMAN,)

    @property
    def require_handler(self):
        return self.type in (ViewFlowTaskTypes.FUNC, ViewFlowTaskTypes.JOB)

    def get_viewflow_definition(self):
        next_nodes = ''
        permission = ''
        definition = self.get_base_definition()
        if self.type == ViewFlowTaskTypes.SWITCH:
            next_activation = '.Case(this.{})'
        else:
            next_activation = '.Next(this.{})'

        if self.next:
            next_nodes = '\n'.join([indent(size=2) + next_activation.format(n.name) for n in self.next])

        if self.require_permission:
            permission = indent(size=2) + '.Permission(auto_create=True)\n'

        sys.stdout.write(definition.format(self.name, self.type, permission, next_nodes))

    def get_base_definition(self):
        if self.type == ViewFlowTaskTypes.END:
            base_definition = indent() + '{} = flow.{}()\n\n'
        else:
            node_args = self.get_node_args()
            base_definition = indent() + '{} = (flow.{}(' + node_args + ')\n{}{})\n\n'

        return base_definition

    def get_node_args(self):
        handler = self.name + '_handler'
        node_args = {
            ViewFlowTaskTypes.START: 'CreateProcessView',
            ViewFlowTaskTypes.HUMAN: 'UpdateProcessView',
            ViewFlowTaskTypes.JOB: handler,
            ViewFlowTaskTypes.FUNC: handler,
            ViewFlowTaskTypes.IF: '',
        }

        if self.require_handler:
            handler_definition = '{}def {}(self, activation):\n{}raise NotImplementedError\n'.format(
                indent(), handler, indent(size=2))
            self.process._handlers.append(handler_definition)

        return node_args.get(self.type, '')


class Sequence(BPMNNode):
    source = None
    target = None

    def parse(self):
        self.source = self.bpmn_node.get('sourceRef')
        self.target = self.bpmn_node.get('targetRef')


def main():
    filename = sys.argv[1]
    bpmn = BPMNFile(filename)
    for key, p in bpmn.processes.items():
        p.get_viewflow_class()


if __name__ == '__main__':
    main()
